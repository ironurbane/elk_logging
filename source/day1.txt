**DAY 1(8 APIs, 17th March)**
=================================================

  .. list-table::
     :widths: 2, 9, 9, 30, 10, 10,10,20
     :header-rows: 1

     * - Task No
       - DEVELOPER
       - CATEGORY
       - BUSINESS FUNCTION
       - Phase
       - Assigned Date
       - Completion Date
       - REMARK 
     * - 1
       - Steve/Dick
       - FLOW ID
       - DEFINE/SETUP ALL FLOW ID CONSTANT IN JAVA AND NAME IN PROPERTY FILE REQUIRED IN ELK LOGGING
       - 1
       - 17th Mar
       - ?
       - 
     * - 2
       - Steve/Dick
       - ACCOUNT
       - CHANGE SUBSCRIBER ACCOUNT CRMS
       - 1
       - 17th Mar
       - 17th Mar
       - 10002=CHANGE SUBSCRIBER ACCOUNT
     * - 3
       - Steve/Dick
       - ADD-ON
       - PURCHASE ADD-ON CRMS eCommerce (Selfcare)
       - 1
       - 17th Mar
       - 17th Mar
       - 11001=PURCHASE ADD ON
     * - 4
       - Steve/Dick
       - ADJUSTMENT
       - PERFORM ADJUSTMENT & IMPOSE CHARGE/OFFER DISCOUNT
       - 1
       - 17th Mar
       - 17th Mar
       - 12001=PERFORM ADJUSTMENT AND IMPOSE CHARGE/OFFER DISCOUNT
     * - 5
       - Steve/Dick
       - AUTOPAY
       - ADD/REMOVE AUTOPAY DMP eCommerce eCommerce (Selfcare) iPay88
       - 1
       - 17th Mar
       - 17th Mar
       - 13001=ADD OR REMOVE AUTOPAY
     * - 6 
       - Steve/Dick
       - BILLING
       - RETRIEVE BILL INFO CRMS DMP eCommerce (Selfcare)
       - 1
       - 17th Mar
       - 17th Mar
       - 14003 
     * - 7
       - Steve/Dick
       - BILLING
       - START BILLING (FIRST SIM INSERTION) ZSmart
       - 1
       - 17th Mar
       - 17th Mar
       - 14001=BILLING_START_BILLING_FIRST_SIM_INSERTION         
     * - 8 
       - Steve/Dick
       - BILLING
       - START BILLING (SUCCESSFUL DELIVERY) DMP
       - 1
       - 17th Mar
       - 17th Mar
       - 14002=BILLING_START_BILLING_SUCCESSFUL_DELIVERY

**Finished by 17th**

