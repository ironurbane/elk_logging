**DAY 3(12 APIs, 21st March)**
=================================================

  .. list-table::
     :widths: 2, 9, 9, 30, 10, 10,10,20
     :header-rows: 1

     * - Task No
       - DEVELOPER
       - CATEGORY
       - BUSINESS FUNCTION
       - Phase
       - Assigned Date
       - Completion Date
       - REMARK 
     * - 1
       - Steve/Dick
       - MNP
       - PORT IN DMP
       - 1
       - 21st Mar
       - 21st Mar
       - 20003=PORT IN
     * - 2
       - Steve/Dick
       - MNP
       - PORT OUT STATUS NOTIFICATION ZSmart
       - 1
       - 21st Mar
       - 21st Mar
       - 20004=PORT OUT STATUS NOTIFICATION
     * - 3
       - Steve/Dick
       - MNP
       - RECYCLE PORT OUT NUMBER ZSmart
       - 1
       - 21st Mar
       - 21st Mar
       - 20005=RECYCLE PORT OUT NUMBER
     * - 4
       - Steve/Dick
       - MNP
       - REJECT PORT IN REQUEST ZSmart
       - 1
       - 21st Mar
       - 21st Mar
       - 20006=REJECT PORT IN REQUEST
     * - 5
       - Steve/Dick
       - MNP
       - RETRIEVE MNP STATUS CRMS DMP IVR
       - 1
       - 21st Mar
       - 21st Mar
       - 20008
     * - 6
       - Steve/Dick
       - NUMBER
       - RESERVE NUMBER (QR) eCommerce
       - 1
       - 21st Mar
       - 21st Mar
       - 21002=NUMBER RESERVATION (QR) 
     * - 7
       - Steve/Dick
       - NUMBER
       - RESERVE NUMBER eCommerce
       - 1
       - 21st Mar
       - 21st Mar
       - 21001=NUMBER RESERVATION
     * - 8
       - Steve/Dick
       - NUMBER
       - EXTEND NUMBER RESERVATION VALIDITY (QR) eCommerce
       - 1
       - 21st Mar
       - 21st Mar
       - 21003=EXTEND NUMBER RESERVATION VALIDITY (QR)
     * - 9
       - Steve/Dick
       - NUMBER
       - RETRIEVE NUMBERS eCommerce
       - 1
       - 21st Mar
       - 21st Mar
       - 21004
     * - 10
       - Steve/Dick
       - PAYMENT
       - RETRIEVE PAYMENT DETAILS DMP
       - 1
       - 21st Mar
       - 21st Mar
       - 23003
     * - 11
       - Steve/Dick
       - PAYMENT
       - SUBMIT PAYMENT TO CONTINUE ORDER DMP
       - 1
       - 21st Mar
       - 21st Mar
       - 23002=SUBMIT PAYMENT TO CONTINUE ORDER
     * - 12
       - Steve/Dick
       - PAYMENT
       - SUBMIT PAYMENT DMP eCommerce(Selfcare)
       - 1
       - 21st Mar
       - 21st Mar
       - 23001=SUBMIT PAYMENT       

**Finished by 21st**

